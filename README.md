# Remove the background from your photos automatically!
<p align="center">
<img src="https://i.imgur.com/lcqFJGe.png">
</p>
* You must get an api key from https://remove.bg


## To install:
```bash
composer require draco-development-group/remove-bg
```

## Quick Start:
### Use the class
```php
// Remember to require the composer autoloader
require_once __DIR__ . '/../vendor/autoload.php';

use DracoDevelopmentGroup\RemoveBg;

$bg = new RemoveBg('YOUR_API_KEY');

$bg->setOutputImageName('path/to/save/new/image/image_name', 'optionalFileType(.jpg or .png)');
$bg->setImageUrl('https://path/to/image.jpg');
$bg->save();
```

### Full Documentation
#### You can set images from urls, or directly upload the image, or give a base64_encoded image:
* You may only use one of the following. If you attempt to use one of these after already setting an image, it will keep the last value you used.
```php
$bg->setImageUrl(string);
$bg->setImageFile(string($binary));
$bg->setImageFileB64(string);
```
#### You can set backgrounds from urls, or directly upload the image, or give a color:
* You may only use one of the following. If you attempt to use one of these after already setting an image, it will keep the last value you used.
```php
$bg->setBgColor(string); //can be hex or color name
$bg->setBgImageUrl(string);
$bg->setBgImageFile(string($binary));
```

#### You can set the following parameters as well
```php
$bg->setSize(string); // preview (default), full, auto, etc
$bg->setType(string); //auto, person, product, car
$bg->setFormat(string); //auto, jpg, png
$bg->setRoi(string); //rectangle as x/y coordinates, <x1> <y1> <x2> <y2>
$bg->setCrop(boolean); //default false
$bg->setChannels(string); //rgba default
```

### Full documentation also available in the class file

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
