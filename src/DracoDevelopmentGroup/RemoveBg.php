<?php


namespace DracoDevelopmentGroup;

use DateTime;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RemoveBg
{

    /**
     * @var $api_key
     *
     * Get this from setting up an account at https://www.remove.bg
     */
    private $api_key;

    /**
     * @var $image_file
     *
     * Source image file (binary).
     * (If this parameter is present, the other image source parameters must be empty.)
     */
    private $image_file;

    /**
     * @var $image_file_b64
     *
     * Source image file (base64-encoded string).
     * (If this parameter is present, the other image source parameters must be empty.)
     */
    private $image_file_b64;

    /**
     * @var $image_url
     *
     * Source image URL.
     * (If this parameter is present, the other image source parameters must be empty.)
     */
    private $image_url;

    /**
     * @var $size
     *
     * Maximum output image resolution: "preview" (default) = Resize image to 0.25 megapixels
     * (e.g. 625×400 pixels – 0.25 credits per image),
     * "full" = Use original image resolution, up to 10 megapixels (e.g. 4000x2500 – 1 credit per image),
     * "auto" = Use highest available resolution (based on image size and available credits).
     * For backwards-compatibility this parameter also accepts the values "medium" (up to 1.5 megapixels)
     * and "hd" (up to 4 megapixels) for 1 credit per image.
     * The value "full" is also available under the name "4k" and the value "preview" is
     * aliased as "small" and "regular".
     */
    private $size;

    /**
     * @var $type
     *
     * Foreground type: "auto" = Automatically detect kind of foreground,
     * "person" = Use person(s) as foreground,
     * "product" = Use product(s) as foreground,
     * "car" = Use car as foreground.
     */
    private $type;

    /**
     * @var $format
     *
     * Result image format: "auto" = Use PNG format if transparent regions exists, otherwise use JPG format (default),
     * "png" = PNG format with alpha transparency,
     * "jpg" = JPG format, no transparency.
     */
    private $format;

    /**
     * @var $roi
     *
     * Region of interest: Only contents of this rectangular region can be detected as foreground.
     * Everything outside is considered background and will be removed.
     * The rectangle is defined as two x/y coordinates in the format "<x1> <y1> <x2> <y2>".
     * The coordinates can be in absolute pixels (suffix 'px') or relative to the width/height of the image (suffix '%').
     * By default, the whole image is the region of interest ("0% 0% 100% 100%").
     */
    private $roi;

    /**
     * @var $crop
     *
     * Whether to crop off all empty regions (default: false).
     * Note that cropping has no effect on the amount of charged credits.
     */
    private $crop;

    /**
     * @var $channels
     *
     * Request either the finalized image ("rgba", default) or an alpha mask ("alpha").
     * Note: Since remove.bg also applies RGB color corrections on edges, using only the alpha mask often leads to
     * a lower final image quality. Therefore "rgba" is recommended.
     */
    private $channels;

    /**
     * @var $bg_color
     *
     * Adds a solid color background. Can be a hex color code (e.g. 81d4fa, fff) or a color name (e.g. green).
     * For semi-transparency, 4-/8-digit hex codes are also supported (e.g. 81d4fa77).
     * (If this parameter is present, the other bg_ parameters must be empty.)
     */
    private $bg_color;

    /**
     * @var $bg_image_url
     *
     * Adds a background image from a URL. The image is centered and resized to fill the canvas while preserving
     * the aspect ratio, unless it already has the exact same dimensions as the foreground image.
     * (If this parameter is present, the other bg_ parameters must be empty.)
     */
    private $bg_image_url;

    /**
     * @var $bg_image_file
     *
     * Adds a background image from a file (binary). The image is centered and resized to fill the canvas while
     * preserving the aspect ratio, unless it already has the exact same dimensions as the foreground image.
     * (If this parameter is present, the other bg_ parameters must be empty.)
     */
    private $bg_image_file;

    /**
     * @var $outputImageName
     *
     * The new image name that you want to create. By default, it will be a random string
     */
    private $outputImageName;

    /**
     * @var $imageType
     *
     * Internal use only - tells us how to format the payload
     */
    private $imageType;

    /**
     * @var $bgImageType
     *
     * Internal use only - tells us how to format the payload
     */
    private $bgImageType;

    /**
     * @var $response
     *
     * This is the response from remove.bg for use in your code
     */
    private $response;

    /**
     * @param mixed $image_file
     */
    public function setImageFile($image_file)
    {
        $this->image_file = $image_file;
        $this->imageType = 'image_file';
        $this->image_file_b64 = null;
        $this->image_url = null;
    }

    /**
     * @param mixed $image_file_b64
     */
    public function setImageFileB64($image_file_b64)
    {
        $this->image_file_b64 = $image_file_b64;
        $this->imageType = 'image_file_b64';
        $this->image_file = null;
        $this->image_url = null;
    }

    /**
     * @param mixed $image_url
     */
    public function setImageUrl($image_url)
    {
        $this->image_url = $image_url;
        $this->imageType = 'image_url';
        $this->image_file = null;
        $this->image_file_b64 = null;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @param mixed $roi
     */
    public function setRoi($roi)
    {
        $this->roi = $roi;
    }

    /**
     * @param mixed $crop
     */
    public function setCrop($crop)
    {
        $this->crop = $crop;
    }

    /**
     * @param mixed $channels
     */
    public function setChannels($channels)
    {
        $this->channels = $channels;
    }

    /**
     * @param mixed $bg_color
     */
    public function setBgColor($bg_color)
    {
        $this->bg_color = $bg_color;
        $this->bgImageType = 'bg_color';
        $this->bg_image_file = null;
        $this->bg_image_url = null;
    }

    /**
     * @param mixed $bg_image_url
     */
    public function setBgImageUrl($bg_image_url)
    {
        $this->bg_image_url = $bg_image_url;
        $this->bgImageType = 'bg_image_url';
        $this->bg_color = null;
        $this->bg_image_file = null;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->api_key;
    }

    /**
     * @param mixed $api_key
     */
    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * @return mixed
     */
    public function getOutputImageName()
    {
        return $this->outputImageName;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @noinspection PhpDocMissingThrowsInspection
     * Create a randomly generated string to output the new file
     * @param null $outputImageName
     * @param string(".png", ".jpg") $outputImageExtension
     *
     */
    public function setOutputImageName($outputImageName = null, $outputImageExtension = ".png")
    {
        if(!$outputImageName) {
            try {
                $this->outputImageName = bin2hex(random_bytes(4)) . "-" . bin2hex(random_bytes(4)) . $outputImageExtension;
            } catch (Exception $e) {
                $this->outputImageName = substr((new DateTime())->format('U') . md5(rand(0, 100)), 0, 10) . $outputImageExtension;
            }
        } else {
            $this->outputImageName = $outputImageName . $outputImageExtension;
        }
    }

    /**
     * @param mixed $bg_image_file
     */
    public function setBgImageFile($bg_image_file)
    {
        $this->bg_image_file = $bg_image_file;
        $this->bgImageType = 'bg_image_file';
        $this->bg_color = null;
        $this->bg_image_url = null;
    }

    public function save(){
        $payload = $this->payload();

        // Check that we have an image to process
        /** @noinspection PhpUnhandledExceptionInspection */
        if($this->allRequiredFieldsPopulated()){
            $request = new Client(['base_uri' => 'https://api.remove.bg/v1.0/removebg']);
            try {
                $this->response = $request->request('POST', '', $payload);
                file_put_contents($this->getOutputImageName(), (string) $this->response->getBody());
            } catch (GuzzleException $e) {
                /** @noinspection PhpUnhandledExceptionInspection */
                $this->error($e->getMessage(), $e->getCode());
            }
        }
    }

    /**
     * This builds the request headers will all populated values
     * @return array
     */
    private function payload(){

        $image = ['name' => $this->imageType, 'contents' => $this->getImageToProcess()];
        $bg = $this->getBgImageToUse() ? ['name' => $this->bgImageType, 'contents' => $this->getBgImageToUse()] : null;
        $size = $this->size ? ['name' => 'size', 'contents' => $this->size] : null;
        $type = $this->type ? ['name' => 'type', 'contents' => $this->type] : null;
        $format = $this->format ? ['name' => 'format', 'contents' => $this->format] : null;
        $roi = $this->roi ? ['name' => 'roi', 'contents' => $this->roi] : null;
        $crop = $this->crop ? ['name' => 'crop', 'contents' => $this->crop] : null;
        $channels = $this->channels ? ['name' => 'channels', 'contents' => $this->channels] : null;

        // build the array of arrays
        $payload[] = $image;

        // These are not required, if null don't include
        if($bg){$payload[] = $bg;}
        if($size){$payload[] = $size;}
        if($type){$payload[] = $type;}
        if($format){$payload[] = $format;}
        if($roi){$payload[] = $roi;}
        if($crop){$payload[] = $crop;}
        if($channels){$payload[] = $channels;}

        return [
            'verify' => false,
            'multipart' => $payload,
            'headers' => [
                'X-Api-Key' => $this->api_key,
            ]
        ];
    }

    /**
     * @return mixed
     * Function checks to see which image type is being used and returns it
     */
    private function getImageToProcess(){
        if($this->image_url){
            return $this->image_url;
        } else if($this->image_file){
            return fopen($this->image_file, 'r');
        } else {
            return  base64_encode(file_get_contents($this->image_file_b64));
        }
    }

    /**
     * @return mixed
     * Function checks to see which image type is being used and returns it
     */
    private function getBgImageToUse(){
        if($this->bg_image_file){
            return $this->bg_image_file;
        } else if ($this->bg_color){
            return $this->bg_color;
        } else {
            return $this->bg_image_url;
        }
    }

    /**
     * Universal error throwing function
     * @param $message
     * @param $code
     * @throws Exception
     */
    private function error($message, $code){
        throw new Exception($message, $code);
    }


    /**
     * Checks to make sure that the API key is valid, and we were provided an image to process.
     * @return bool
     * @throws Exception
     */
    private function allRequiredFieldsPopulated(){

        $client = new Client();

        // Check that an image has been set
        if(!$this->getImageToProcess()){
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->error('You did not provide an image to process. You must use either setImageFile, setImageFileB64, or setImageUrl before calling process()', 400);
        }

        // Check the API key
        try{
            $client->get('https://api.remove.bg/v1.0/account', [
                'headers' => [
                    'X-Api-Key' => $this->api_key,
                ]
            ]);
        } catch (Exception $e){
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->error('Invalid API key. Please make sure you copy and paste your key as a string exactly as it is found at https://www.remove.bg/profile#api-key', 400);
        }

        // If we make it this far, everything is populated
        return true;
    }


    /**
     * Sets default values for the library
     * RemoveBg constructor.
     * @param $api_key
     */
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->setOutputImageName();
    }
}
